window.Zap = window.Zap || {};

(function ($, window, document, undefined) {
    'use strict';

    var $doc = $(document),
        $win = $(window),
        $root = $('html, body'),

        HamburgersHandler = {
            watch: function () {
                $('.hamburger').on('click', function () {
                    $(this).toggleClass('is-active');
                });
            }
        },

        ViewSwitcherHandler = {
            $viewEl: $('.real-estate-list'),

            watch: function ($elements) {
                $elements.on('click', function () {
                    var $this = $(this);

                    if (!$this.parent().hasClass('is-active'))
                        ViewSwitcherHandler.changeState($this);
                });
            },

            changeState: function ($el) {
                $el.parent().addClass('is-active').siblings().removeClass('is-active');
                this.$viewEl.toggleClass('list-view gallery-view');
            }
        },

        SliderHandler = {
            init: function ($el) {
                $el.slick({
                    slidesToShow: 2,
                    slidesToScroll: 1
                });
            }
        },

        FavoriteHandler = {
            watch: function ($elements) {
                $elements.on('click', function () {
                    FavoriteHandler.changeState($(this));
                });
            },

            changeState: function ($el) {
                $el.toggleClass('is-active');

                if ($el.hasClass('real-estate__favorite--star')) {
                    var $listViewButton = $el.prev().find('.real-estate__favorite');
                    $listViewButton.toggleClass('is-active');
                    $listViewButton.text($listViewButton.hasClass('is-active') ? 'Remover dos favoritos' : 'Adicionar aos favoritos');
                } else {
                    var $galleryViewButton = $el.parent().next();
                    $el.text($el.hasClass('is-active') ? 'Remover dos favoritos' : 'Adicionar aos favoritos');
                    $galleryViewButton.toggleClass('is-active');
                }
            }
        };

    Zap.app = function () {
        return {
            init: function () {
                var $switcherViewButtons = $('.view-switcher__option'),
                    $favoriteButtons = $('.real-estate__favorite'),
                    $gallerySlider = $('.real-estate__slider');

                HamburgersHandler.watch();

                if ($switcherViewButtons.length)
                    ViewSwitcherHandler.watch($switcherViewButtons);

                if ($favoriteButtons.length)
                    FavoriteHandler.watch($favoriteButtons);

                if ($gallerySlider.length)
                    SliderHandler.init($gallerySlider);
            }
        };
    };

})(jQuery, window, document, undefined);

zapTest = new Zap.app();
zapTest.init();
