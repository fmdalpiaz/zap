'use strict';

var browserSync = require('browser-sync').create(),
    gulp = require('gulp'),
    autoPrefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify-css'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    gUtil = require('gulp-util');

var browsersList = ['last 2 versions', '> 5%'];

var errorsHandler = function (error) {
    gUtil.log(gUtil.colors.red(error.message));
    gUtil.beep();
    this.emit('end');
};

gulp.task('server', function () {
    browserSync.init({
        server: { baseDir: 'site' },
        reloadDelay: 200,
        notify: false
    });

    gulp.watch('**/*.html').on('change', function () {
        browserSync.reload();
    });
});

gulp.task('styles', function () {
    return gulp.src('site/_styles/_sass/styles.scss')
        .pipe(plumber({ errorHandler: errorsHandler }))
        .pipe(sass({ errLogToConsole: true }))
        .pipe(autoPrefixer({ browsers: browsersList, casacade: true }))
        .pipe(minify())
        .pipe(gulp.dest('site/_styles'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('scripts', function() {
    return gulp.src('site/_scripts/_js/**/*.js')
        .pipe(plumber({ errorHandler: errorsHandler }))
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('site/_scripts'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('default', ['server', 'styles', 'scripts'], function () {
    gulp.watch('site/_styles/_sass/**', ['styles']);
    gulp.watch('site/_scripts/_js/**', ['scripts']);
});
