# SUB100 Imóveis
## Teste do processo seletivo

### Executando o projeto

Existem duas maneiras de ver a solução rodando:

1. Abrir o arquivo ___/site/index.html___
2. Através do ___Gulp___, para isso será necessário:
    1. Rodar o comando ___npm install___
    2. Após a instalação dos pacotes, rodar o comando ___gulp___
    3. O ___Gulp___ está configurado com o ___BrowserSync___, ou seja, ele irá abrir o browser rodando um servidor local
    4. O projeto foi desenvolvido no ___OSx___, talvez exista alguma incompatibilidade com sistemas Windows.

### Observações sobre o teste:

1. Utilizei o sistema de grid do ___Bootstrap___. Com isso, além do breakpoint descrito no e-mail (___940px___), o teste conta com os breakpoints: ___768px___ e ___1200px___.
2. Utilizei ___jQuery___ para realizar as tarefas simples e dar suporte ao plugin de slider utilizado.
3. Não criei o slider, utilizei um plugin famoso por sua versatilidade, performance e suporte, chamado [_Slick Slider_](https://github.com/kenwheeler/slick).

* Eu poderia ter criado slider, mas não vi motivos para tal dada a gama de soluções excelentes disponíveis na comunidade. Faz parte do trabalho do desenvolvedor identificar quando algo precisa ser criado do zero ou quando buscar soluções sólidas. Se eu fosse criar um slider, com certeza ele não seria tão bom quanto uma ferramenta que já é desenvolvida pela comunidade há um bom tempo.
